#!/bin/bash
SRC="goodNOOUTPUT.cu"
EXE="gNOOUTPUTexe"


I1=32
I2=64
I4=128
I8=256
I16=512
I32=1024

X="./"
echo $I1
time "$X$EXE$I1" h100M.csv tmp.json
echo $I2
time "$X$EXE$I2" h100M.csv tmp.json
echo $I4
time "$X$EXE$I4" h100M.csv tmp.json
echo $I8
time "$X$EXE$I8" h100M.csv tmp.json
echo $I16
time "$X$EXE$I16" h100M.csv tmp.json
echo $I32
time "$X$EXE$I32" h100M.csv tmp.json
