#include <iostream>
#include <iomanip>
#include <cstdlib>
using namespace std;
int main(int argc, char *argv[])
{
	int Z = atoi(argv[1]);

	const int N = 16;
	const int A = 15, B = 31;
	string a, b;
	for (int i = 0; i < N; ++i) {
		if (i) a += ",";
		string s;
		s += 'A' + i;
		for (int j = 0; s.size() < A; ++j)
			s += ((j & 1) ? 'A' : 'a') + i;
		a += s;
	}
	cout << a << "\n";
	for (int z = 0, q = 0; z < Z; ++z) {
		for (int i = 0; i < N; ++i) {
			if (i) cout << ",";
			cout << char('A' + i) << setfill('0') << setw(B - 1) << (q++);
		}
		cout << "\n";
	}
	return 0;
}
