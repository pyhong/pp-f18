#!/bin/bash
SRC="goodNOOUTPUT.cu"
EXE="gNOOUTPUTexe"


I1=32
I2=64
I4=128
I8=256
I16=512
I32=1024

nvcc $SRC -std=c++11 -O3 -D_FORCE_INLINES -lineinfo -o "$EXE$I1" -DDEBUGXT=$I1
nvcc $SRC -std=c++11 -O3 -D_FORCE_INLINES -lineinfo -o "$EXE$I2" -DDEBUGXT=$I2
nvcc $SRC -std=c++11 -O3 -D_FORCE_INLINES -lineinfo -o "$EXE$I4" -DDEBUGXT=$I4
nvcc $SRC -std=c++11 -O3 -D_FORCE_INLINES -lineinfo -o "$EXE$I8" -DDEBUGXT=$I8
nvcc $SRC -std=c++11 -O3 -D_FORCE_INLINES -lineinfo -o "$EXE$I16" -DDEBUGXT=$I16
nvcc $SRC -std=c++11 -O3 -D_FORCE_INLINES -lineinfo -o "$EXE$I32" -DDEBUGXT=$I32


#X="./"
#echo $I1
#time "$X$EXE$I1" h100M.csv tmp.json
