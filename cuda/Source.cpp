
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <chrono>

using namespace std;

bool istrue(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 't' && s[1] == 'r' && s[2] == 'u' && s[3] == 'e';
}
bool isfalse(const char *s, size_t slen)
{
	if (slen != 5)
		return false;
	return s[0] == 'f' && s[1] == 'a' && s[2] == 'l' && s[3] == 's' && s[4] == 'e';
}
bool isnull(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 'n' && s[1] == 'u' && s[2] == 'l' && s[3] == 'l';
}
bool isnumber(const char *s, size_t slen)
{
	int n = 0;
	bool d = false, z = false;
	if (0 < slen && s[0] == '-')
		n = 1;
	if (n < slen && s[n] == '0') {
		++n;
		if (n != slen)
			return false;
	}

	for (int i = n; i < slen; ++i)
		if (s[i] == '.') {
			if (d)
				return false;
			d = true;
		}
		else if (!('0' <= s[i] && s[i] <= '9'))
			return false;
	return true;
}
bool isstring(const char *s, size_t slen)
{
	return !(istrue(s, slen) || isfalse(s, slen) || isnull(s, slen) || isnumber(s, slen));
}

const int AA = 25, BB = 128;

void splitstrbycomma(const char *s, size_t slen, char buf[AA][BB])
{
	//char buf[32][2048] = {};
	size_t buflen = 0;

	for (int i = 0; i < slen; ++i) {
		size_t sleft = i, sright = -1;
		bool q = false;
		while (i < slen) {
			if (s[i] == '"')
				q = !q;
			else if (s[i] == ',' || s[i] == '\0') {
				if (!q) {
					sright = i;

					if (isstring(s + sleft, sright - sleft)) {
						//cout << "#";
						if (s[sleft] == '"')
							++sleft;
						if (s[sright - 1] == '"')
							--sright;
						buf[buflen][0] = '"';
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft + 1] = s[j];
						buf[buflen][sright - sleft + 1] = '"';
					}
					else {
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft] = s[j];
					}
					/*cout << "[@ ";
					for (int j = 0; buf[buflen][j]; ++j)
						cout << buf[buflen][j];
					cout << " @]" << endl;*/

					++buflen;
					sleft = i + 1;
					break;
				}
			}
			++i;
		}
	}
}

size_t csv2json(const char *s, size_t slen, char field[AA][BB], size_t fieldlen, char *json)
{
	char record[AA][BB] = {};
	splitstrbycomma(s, slen, record);
	//cout << "{\n";
	size_t jsonlen = 0;
	json[jsonlen++] = ' ';
	json[jsonlen++] = '{';
	json[jsonlen++] = '\n';
	for (int i = 0; i < fieldlen; ++i) {
		json[jsonlen++] = ' ';
		json[jsonlen++] = ' ';
		for (int j = 0; field[i][j]; ++j)
			json[jsonlen++] = field[i][j];
		json[jsonlen++] = ':';
		json[jsonlen++] = ' ';
		for (int j = 0; record[i][j]; ++j)
			json[jsonlen++] = record[i][j];
		if (i + 1 < fieldlen)
			json[jsonlen++] = ',';
		json[jsonlen++] = '\n';
	}
	json[jsonlen++] = ' ';
	json[jsonlen++] = '}';
	return jsonlen;
}

int main(int argc, char *argv[])
{
	ifstream fin(argv[1]);
	string s;
	getline(fin, s);
	char field[AA][BB] = {};
	splitstrbycomma(s.c_str(), s.size() + 1, field);
	size_t fieldlen = 0;
	while (field[fieldlen][0])
		++fieldlen;
	
	vector<string> vrecords;
	while (getline(fin, s)) {
		vrecords.push_back(s);
	}

	ofstream fout(argv[2]);
	fout << "[\n";

	auto start = chrono::system_clock::now();
	// TODO: parallel this for loop
	for (int i = 0; i < vrecords.size(); ++i) {
		if (i) fout << ",\n";
		
		char json[1024] = {};
		size_t jsonlen = csv2json(vrecords[i].c_str(), vrecords[i].size() + 1, field, fieldlen, json);

		fout << json;
	}
	fout << "\n]\n";
	auto end = chrono::system_clock::now();
	chrono::duration<double> diff = end - start;
	cout << "time: " << diff.count() << "s" << endl;

	fin.close();
	fout.close();

	return 0;
}
