
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <chrono>

using namespace std;

__host__ __device__ bool istrue(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 't' && s[1] == 'r' && s[2] == 'u' && s[3] == 'e';
}
__host__ __device__ bool isfalse(const char *s, size_t slen)
{
	if (slen != 5)
		return false;
	return s[0] == 'f' && s[1] == 'a' && s[2] == 'l' && s[3] == 's' && s[4] == 'e';
}
__host__ __device__ bool isnull(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 'n' && s[1] == 'u' && s[2] == 'l' && s[3] == 'l';
}
__host__ __device__ bool isnumber(const char *s, size_t slen)
{
	int n = 0;
	bool d = false;
	if (0 < slen && s[0] == '-')
		n = 1;
	if (n < slen && s[n] == '0') {
		++n;
		if (n != slen)
			return false;
	}

	for (int i = n; i < slen; ++i)
		if (s[i] == '.') {
			if (d)
				return false;
			d = true;
		}
		else if (!('0' <= s[i] && s[i] <= '9'))
			return false;
	return true;
}
__host__ __device__ bool isstring(const char *s, size_t slen)
{
	return !(istrue(s, slen) || isfalse(s, slen) || isnull(s, slen) || isnumber(s, slen));
}

const int AA = 32, BB = 128;

__host__ __device__ void splitstrbycomma(const char *s, size_t slen, char buf[AA][BB])
{
	//char buf[32][2048] = {};
	size_t buflen = 0;

	for (size_t i = 0; i < slen; ++i) {
		size_t sleft = i, sright = -1;
		bool q = false;
		while (i < slen) {
			if (s[i] == '"')
				q = !q;
			else if (s[i] == ',' || s[i] == '\0') {
				if (!q) {
					sright = i;

					if (isstring(s + sleft, sright - sleft)) {
						//cout << "#";
						if (s[sleft] == '"')
							++sleft;
						if (s[sright - 1] == '"')
							--sright;
						buf[buflen][0] = '"';
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft + 1] = s[j];
						buf[buflen][sright - sleft + 1] = '"';
						buf[buflen][sright - sleft + 2] = '\0';
					}
					else {
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft] = s[j];
						buf[buflen][sright - sleft] = '\0';
					}
					/*cout << "[@ ";
					for (int j = 0; buf[buflen][j]; ++j)
						cout << buf[buflen][j];
					cout << " @]" << endl;*/

					++buflen;
					sleft = i + 1;
					break;
				}
			}
			++i;
		}
	}
}

__device__ size_t csv2json(const char *s, size_t slen, char *f, size_t flen, size_t *fidx, size_t fidxlen, char *json)
{
	char record[AA][BB];// = {};
        splitstrbycomma(s, slen, record);
        size_t jsonlen = 0;
        json[jsonlen++] = ' ';
        json[jsonlen++] = '{';
        json[jsonlen++] = '\n';
        for (int i = 0; i < fidxlen; ++i) {
                json[jsonlen++] = ' ';
                json[jsonlen++] = ' ';
                for (int j = fidx[i]; f[j]; ++j)
                        json[jsonlen++] = f[j];
                json[jsonlen++] = ':';
                json[jsonlen++] = ' ';
                for (int j = 0; record[i][j]; ++j)
                        json[jsonlen++] = record[i][j];
                if (i + 1 < fidxlen)
                        json[jsonlen++] = ',';
                json[jsonlen++] = '\n';
        }
        json[jsonlen++] = ' ';
        json[jsonlen++] = '}';
        return jsonlen;
}

const int T = 320;
const int64_t JL = DEBUGJL;//1000000000LL;//1LL << 32;

__global__ void kernel(char *S, size_t Slen, char *F, size_t Flen, size_t *Fidx, size_t Fidxlen, char *J)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	size_t w = Slen / T;
	size_t L = w * idx;
	size_t R = w * (idx + 1);
	if (idx == T - 1) R = Slen;
	if (L > 0) {
		if (S[L - 1] == '\n') ;
		else {
			while (L < R && S[L] != '\n') ++L;
			++L;
		}
	}
	if (R > L) while (R <= Slen && S[R - 1] != '\n') ++R;

	size_t sL = L, sR = R;

	char *json = J + (JL / T) * idx;
	size_t Jlen = 0;
	for (size_t i = L; i < R; ++i) {
		if (S[i] == '\n') {
			S[i] = '\0';
			sR = i;
			if (sL) {
				json[Jlen++] = ',';
                        	json[Jlen++] = '\n';
			}
			Jlen += csv2json(S + sL, sR - sL + 1, F, Flen, Fidx, Fidxlen, json + Jlen);
			sL = i + 1;
		}
	}
	json[Jlen++] = '\0';
}

int main(int argc, char *argv[])
{
	ifstream fin(argv[1]);
	string s;
	getline(fin, s);
	char field[AA][BB] = {};
	splitstrbycomma(s.c_str(), s.size() + 1, field);
	size_t fieldlen = 0;
	while (field[fieldlen][0])
		++fieldlen;
	{
		char f[AA * BB] = {};
		size_t fidx[AA] = {};
		size_t flen = 0;
		for (int i = 0; i < fieldlen; ++i) {
			fidx[i] = flen;
			for (int j = 0; field[i][j]; ++j)
				f[flen++] = field[i][j];
			f[flen++] = '\0';
		}
		fidx[fieldlen] = flen;
		f[flen++] = '\0';
		cout << "$ " << fieldlen << endl;
		for (int i = 0; i < fieldlen; ++i)
			cout << "% " << (f+fidx[i]) << endl;

		ostringstream ss;
		ss << fin.rdbuf();
		string s = ss.str();
		cout << "# " << s.size() << endl;

		auto start = chrono::system_clock::now();

		char *dS, *dF, *dJ;
		size_t *dFidx;
		cudaMalloc(&dS, s.size());
		cudaMalloc(&dJ, JL);
		cudaMalloc(&dF, flen);
		cudaMalloc(&dFidx, fieldlen * sizeof(size_t));
		char *J = new char [JL];
		cudaMemcpy(dS, s.c_str(), s.size(), cudaMemcpyHostToDevice);
		cudaMemcpy(dF, f, flen, cudaMemcpyHostToDevice);
		cudaMemcpy(dFidx, fidx, fieldlen * sizeof(size_t), cudaMemcpyHostToDevice);
		kernel<<<T/32, 32>>>(dS, s.size(), dF, flen, dFidx, fieldlen, dJ);

		cudaMemcpy(J, dJ, JL, cudaMemcpyDeviceToHost);

		ofstream fout(argv[2]);
		fout << "[\n";
		const int w = (JL / T);
		for (int i = 0; i < T; ++i)
			fout << (J + w * i);
		fout << "\n]\n";
		delete[] J;

		fin.close();
		fout.close();
		cudaFree(dS);
                cudaFree(dF);
                cudaFree(dJ);
                cudaFree(dFidx);

	auto end = chrono::system_clock::now();
	chrono::duration<double> diff = end - start;
	cout << "time: " << diff.count() << "s" << endl;

	}
	return 0;
}
