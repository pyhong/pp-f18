
#include <fstream>
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <chrono>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;
const int AA = 8, BB = 8, CC = 128;
const long long int DD = 8000000000LL;
//global
char field[AA][BB] = {};  //save the key
vector<string> vrecords;
int num_threads = 0;
int vector_size=0;
size_t fieldlen = 0;       //number of keys
char *threadbuffer[8];//[500000000];

bool istrue(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 't' && s[1] == 'r' && s[2] == 'u' && s[3] == 'e';
}
bool isfalse(const char *s, size_t slen)
{
	if (slen != 5)
		return false;
	return s[0] == 'f' && s[1] == 'a' && s[2] == 'l' && s[3] == 's' && s[4] == 'e';
}
bool isnull(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 'n' && s[1] == 'u' && s[2] == 'l' && s[3] == 'l';
}
bool isnumber(const char *s, size_t slen)
{
	int n = 0;
	bool d = false, z = false;
	if (0 < slen && s[0] == '-')
		n = 1;
	if (n < slen && s[n] == '0') {
		++n;
		if (n != slen)
			return false;
	}

	for (int i = n; i < slen; ++i)
		if (s[i] == '.') {
			if (d)
				return false;
			d = true;
		}
		else if (!('0' <= s[i] && s[i] <= '9'))
			return false;
	return true;
}
bool isstring(const char *s, size_t slen)
{
	return !(istrue(s, slen) || isfalse(s, slen) || isnull(s, slen) || isnumber(s, slen));
}

void splitstrbycomma(const char *s, size_t slen, char buf[AA][BB])
{
	//char buf[32][2048] = {};
	size_t buflen = 0;

	for (int i = 0; i < slen; ++i) {
		size_t sleft = i, sright = -1;
		bool q = false;
		while (i < slen) {
			if (s[i] == '"')
				q = !q;
			else if (s[i] == ',' || s[i] == '\0') {
				if (!q) {
					sright = i;

					if (isstring(s + sleft, sright - sleft)) {
						//cout << "#";
						if (s[sleft] == '"')
							++sleft;
						if (s[sright - 1] == '"')
							--sright;
						buf[buflen][0] = '"';
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft + 1] = s[j];
						buf[buflen][sright - sleft + 1] = '"';
					}
					else {
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft] = s[j];
					}
					/*cout << "[@ ";
					for (int j = 0; buf[buflen][j]; ++j)
						cout << buf[buflen][j];
					cout << " @]" << endl;*/

					++buflen;
					sleft = i + 1;
					break;
				}
			}
			++i;
		}
	}
}

size_t csv2json(const char *s, size_t slen, char field[AA][BB], size_t fieldlen, char *json)
{
	char record[AA][BB] = {};
	splitstrbycomma(s, slen, record);
	//cout << "{\n";
	size_t jsonlen = 0;
//	json[jsonlen++] = ' ';
//	json[jsonlen++] = ' ';
	json[jsonlen++] = '{';
	json[jsonlen++] = '\n';
	for (int i = 0; i < fieldlen; ++i) {
//		json[jsonlen++] = ' ';
//		json[jsonlen++] = ' ';
//		json[jsonlen++] = ' ';
//		json[jsonlen++] = ' ';
		for (int j = 0; field[i][j]; ++j)
			json[jsonlen++] = field[i][j];
		json[jsonlen++] = ':';
//		json[jsonlen++] = ' ';
		for (int j = 0; record[i][j]; ++j)
			json[jsonlen++] = record[i][j];
		if (i + 1 < fieldlen)
			json[jsonlen++] = ',';
		json[jsonlen++] = '\n';
	}
//	json[jsonlen++] = ' ';
//	json[jsonlen++] = ' ';
	json[jsonlen++] = '}';
	return jsonlen;
}

void *convert(void *data);

int main(int argc, char *argv[])
{

	ifstream fin(argv[1]);    //open csv file
	string s;
	getline(fin, s);          //read key line
	splitstrbycomma(s.c_str(), s.size() + 1, field);  //split key to field
	while (field[fieldlen][0])
		++fieldlen;
        //auto start = chrono::system_clock::now();
	//vector<string> vrecords;    move to global
	while (getline(fin, s)) {   //read the rest of the csv
		vrecords.push_back(s);
	}
        vector_size=vrecords.size();
        ofstream fout(argv[2]);
	fout << "[\n";
//	auto read = chrono::system_clock::now();
//	chrono::duration<double> diff ;
	//cout << "read_time: " << diff.count() << "s" << endl;
	// TODO: parallel this for loop --pthread
	//int num_threads = 0;     move to global
	num_threads= atoi(argv[3]);
	long long int Q = DD / num_threads;
	for(int i=0;i<num_threads;i++){
		threadbuffer[i] = new char [Q];//[1000000000];
		memset(threadbuffer[i], 0, Q);//1000000000);
	}
	pthread_t tid[num_threads];
	int j =0;
        //auto read = chrono::system_clock::now();
        //chrono::duration<double> diff ;
	for(j=0;j<num_threads;j++){

	   if(pthread_create( &tid[j], NULL, &convert,(void *)j) != 0){
	     perror("create thread failed");
	       }
	    }
        for (j = 0; j < num_threads; j++){
            pthread_join(tid[j] , NULL);
        }
	//auto mid = chrono::system_clock::now();
	//diff = mid - read;
	//cout << "parse_time: " << diff.count() << "s" << endl;
	///////////////////////////////////////////////////////
	for (int z = 0; z < num_threads; z++){
           fout<<threadbuffer[z];
           delete[] threadbuffer[z];
        }
	//////////////////////////////////////////////////////
	fout << "]\n";
	//auto end = chrono::system_clock::now();
	//diff = end - mid;
	//cout << "write_time: " << diff.count() << "s" << endl;
	fin.close();
	fout.close();
	//for(int i=0;i<num_threads;i++){
	//	delete[] threadbuffer[i];
	//}

	return 0;
}

//pthread
void *convert(void *data){
	string tmpstring;
	long long int order = (long long int)data;
	int counter= vector_size/num_threads;
	long long int buffercount=0;
	if(num_threads==(order+1)){
		for (int i = order*counter; i < vrecords.size(); ++i) {
			if (i){
				memcpy (&threadbuffer[order][buffercount], ",\n", 2 );
				buffercount=buffercount+2;
			}
			char json[CC] = {};
			size_t jsonlen = csv2json(vrecords[i].c_str(), vrecords[i].size() + 1, field, fieldlen, json);
			memcpy (&threadbuffer[order][buffercount], json, jsonlen );
			buffercount=buffercount+jsonlen;
		}
	}
	else{
		for (int i = order*counter; i < (order+1)*counter; ++i) {
			if (i){
				memcpy (&threadbuffer[order][buffercount], ",\n", 2 );
				buffercount=buffercount+2;
			}
			char json[CC] = {};
			size_t jsonlen = csv2json(vrecords[i].c_str(), vrecords[i].size() + 1, field, fieldlen, json);
			memcpy (&threadbuffer[order][buffercount], json, jsonlen );
			buffercount=buffercount+jsonlen;
		}
	}
  pthread_exit(NULL);
}
