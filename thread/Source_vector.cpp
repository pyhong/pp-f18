
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <pthread.h>
#include <stdlib.h>

using namespace std;

//global
const int AA=8,BB=8,CC=128;
char field[AA][BB] = {};  //save the key
vector<string> vrecords;
int num_threads = 0;
int vector_size=0;
size_t fieldlen = 0;       //number of keys
vector<string> tmpvector[10];

bool istrue(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 't' && s[1] == 'r' && s[2] == 'u' && s[3] == 'e';
}
bool isfalse(const char *s, size_t slen)
{
	if (slen != 5)
		return false;
	return s[0] == 'f' && s[1] == 'a' && s[2] == 'l' && s[3] == 's' && s[4] == 'e';
}
bool isnull(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 'n' && s[1] == 'u' && s[2] == 'l' && s[3] == 'l';
}
bool isnumber(const char *s, size_t slen)
{
	int n = 0;
	bool d = false, z = false;
	if (0 < slen && s[0] == '-')
		n = 1;
	if (n < slen && s[n] == '0') {
		++n;
		if (n != slen)
			return false;
	}

	for (int i = n; i < slen; ++i)
		if (s[i] == '.') {
			if (d)
				return false;
			d = true;
		}
		else if (!('0' <= s[i] && s[i] <= '9'))
			return false;
	return true;
}
bool isstring(const char *s, size_t slen)
{
	return !(istrue(s, slen) || isfalse(s, slen) || isnull(s, slen) || isnumber(s, slen));
}

void splitstrbycomma(const char *s, size_t slen, char buf[AA][BB])
{
	//char buf[32][2048] = {};
	size_t buflen = 0;

	for (int i = 0; i < slen; ++i) {
		size_t sleft = i, sright = -1;
		bool q = false;
		while (i < slen) {
			if (s[i] == '"')
				q = !q;
			else if (s[i] == ',' || s[i] == '\0') {
				if (!q) {
					sright = i;

					if (isstring(s + sleft, sright - sleft)) {
						//cout << "#";
						if (s[sleft] == '"')
							++sleft;
						if (s[sright - 1] == '"')
							--sright;
						buf[buflen][0] = '"';
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft + 1] = s[j];
						buf[buflen][sright - sleft + 1] = '"';
					}
					else {
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft] = s[j];
					}
					/*cout << "[@ ";
					for (int j = 0; buf[buflen][j]; ++j)
						cout << buf[buflen][j];
					cout << " @]" << endl;*/

					++buflen;
					sleft = i + 1;
					break;
				}
			}
			++i;
		}
	}
}

size_t csv2json(const char *s, size_t slen, char field[AA][BB], size_t fieldlen, char *json)
{
	char record[AA][BB] = {};
	splitstrbycomma(s, slen, record);
	//cout << "{\n";
	size_t jsonlen = 0;
	json[jsonlen++] = ' ';
	json[jsonlen++] = ' ';
	json[jsonlen++] = '{';
	json[jsonlen++] = '\n';
	for (int i = 0; i < fieldlen; ++i) {
		json[jsonlen++] = ' ';
		json[jsonlen++] = ' ';
		json[jsonlen++] = ' ';
		json[jsonlen++] = ' ';
		for (int j = 0; field[i][j]; ++j)
			json[jsonlen++] = field[i][j];
		json[jsonlen++] = ':';
		json[jsonlen++] = ' ';
		for (int j = 0; record[i][j]; ++j)
			json[jsonlen++] = record[i][j];
		if (i + 1 < fieldlen)
			json[jsonlen++] = ',';
		json[jsonlen++] = '\n';
	}
	json[jsonlen++] = ' ';
	json[jsonlen++] = ' ';
	json[jsonlen++] = '}';
	return jsonlen;
}

void *convert(void *data);

int main(int argc, char *argv[])
{
	ifstream fin(argv[1]);    //open csv file
	string s;
	getline(fin, s);          //read key line
	//char field[32][2048] = {};  //save the key  move to global
	splitstrbycomma(s.c_str(), s.size() + 1, field);  //split key to field
	//size_t fieldlen = 0;       //number of keys  move to global
	while (field[fieldlen][0])
		++fieldlen;

	//vector<string> vrecords;    move to global
	while (getline(fin, s)) {   //read the rest of the csv
		vrecords.push_back(s);
	}
  vector_size=vrecords.size();
	ofstream fout(argv[2]);    //open json file

	fout << "[\n";
	auto start = chrono::system_clock::now();
	// TODO: parallel this for loop --pthread
	//int num_threads = 0;     move to global
	num_threads= atoi(argv[3]);
	pthread_t tid[num_threads];
	int j =0;

	for(j=0;j<num_threads;j++){

	   if(pthread_create( &tid[j], NULL, &convert,(void *)j) != 0){
	     perror("create thread failed");
	       }
	    }
  for (j = 0; j < num_threads; j++){
      pthread_join(tid[j] , NULL);
  }
	for (j = 0; j < num_threads; j++){
      for (int i = 0; i < tmpvector[j].size(); ++i) {
          fout<<tmpvector[j][i].c_str();
			}
  }

	//////////////////////////////////////////////////////
	fout << "]\n";
	auto end = chrono::system_clock::now();
	chrono::duration<double> diff = end - start;
	cout << "time: " << diff.count() << "s" << endl;

	fin.close();
	fout.close();

	return 0;
}

//pthread
void *convert(void *data){
	string tmpstring;
	long long int order = (long long int)data;
	int counter= vector_size/num_threads;
	if(num_threads==(order+1)){
		for (int i = order*counter; i < vrecords.size(); ++i) {
			if (i){
				tmpstring=",\n";
				tmpvector[order].push_back(tmpstring);
			}
			char json[CC] = {};
			size_t jsonlen = csv2json(vrecords[i].c_str(), vrecords[i].size() + 1, field, fieldlen, json);
			tmpstring=json;
			tmpvector[order].push_back(tmpstring);
		}
	}
	else{
		for (int i = order*counter; i < (order+1)*counter; ++i) {
			if (i){
				tmpstring=",\n";
				tmpvector[order].push_back(tmpstring);
			}

			char json[CC] = {};
			size_t jsonlen = csv2json(vrecords[i].c_str(), vrecords[i].size() + 1, field, fieldlen, json);

			tmpstring=json;
		  tmpvector[order].push_back(tmpstring);
		}
	}
  pthread_exit(NULL);
}
