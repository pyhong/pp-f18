
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <string.h>
#include <chrono>

#define TNUM 16
using namespace std;

const int AA = 8, BB = 8, CC = 128;
bool istrue(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 't' && s[1] == 'r' && s[2] == 'u' && s[3] == 'e';
}
bool isfalse(const char *s, size_t slen)
{
	if (slen != 5)
		return false;
	return s[0] == 'f' && s[1] == 'a' && s[2] == 'l' && s[3] == 's' && s[4] == 'e';
}
bool isnull(const char *s, size_t slen)
{
	if (slen != 4)
		return false;
	return s[0] == 'n' && s[1] == 'u' && s[2] == 'l' && s[3] == 'l';
}
bool isnumber(const char *s, size_t slen)
{
	int n = 0;
	bool d = false, z = false;
	if (0 < slen && s[0] == '-')
		n = 1;
	if (n < slen && s[n] == '0') {
		++n;
		if (n != slen)
			return false;
	}

	for (int i = n; i < slen; ++i)
		if (s[i] == '.') {
			if (d)
				return false;
			d = true;
		}
		else if (!('0' <= s[i] && s[i] <= '9'))
			return false;
	return true;
}
bool isstring(const char *s, size_t slen)
{
	return !(istrue(s, slen) || isfalse(s, slen) || isnull(s, slen) || isnumber(s, slen));
}

void splitstrbycomma(const char *s, size_t slen, char buf[AA][BB])
{
	//char buf[AA][BB] = {};
	size_t buflen = 0;

	for (int i = 0; i < slen; ++i) {
		size_t sleft = i, sright = -1;
		bool q = false;
		while (i < slen) {
			if (s[i] == '"')
				q = !q;
			else if (s[i] == ',' || s[i] == '\0') {
				if (!q) {
					sright = i;

					if (isstring(s + sleft, sright - sleft)) {
						//cout << "#";
						if (s[sleft] == '"')
							++sleft;
						if (s[sright - 1] == '"')
							--sright;
						buf[buflen][0] = '"';
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft + 1] = s[j];
						buf[buflen][sright - sleft + 1] = '"';
					}
					else {
						for (int j = sleft; j < sright; ++j)
							buf[buflen][j - sleft] = s[j];
					}
					/*cout << "[@ ";
					for (int j = 0; buf[buflen][j]; ++j)
						cout << buf[buflen][j];
					cout << " @]" << endl;*/

					++buflen;
					sleft = i + 1;
					break;
				}
			}
			++i;
		}
	}
}

size_t csv2json(const char *s, size_t slen, char field[AA][BB], size_t fieldlen, char *json)
{
	char record[AA][BB] = {};
	splitstrbycomma(s, slen, record);
	//cout << "{\n";
	size_t jsonlen = 0;
//	json[jsonlen++] = ' ';
//	json[jsonlen++] = ' ';
	json[jsonlen++] = '{';
	json[jsonlen++] = '\n';
	for (int i = 0; i < fieldlen; ++i) {
//		json[jsonlen++] = ' ';
//		json[jsonlen++] = ' ';
//		json[jsonlen++] = ' ';
//		json[jsonlen++] = ' ';
		for (int j = 0; field[i][j]; ++j)
			json[jsonlen++] = field[i][j];
		json[jsonlen++] = ':';
//		json[jsonlen++] = ' ';
		for (int j = 0; record[i][j]; ++j)
			json[jsonlen++] = record[i][j];
		if (i + 1 < fieldlen)
			json[jsonlen++] = ',';
		json[jsonlen++] = '\n';
	}
//	json[jsonlen++] = ' ';
//	json[jsonlen++] = ' ';
	json[jsonlen++] = '}';
	json[jsonlen] = '\0';
	return jsonlen;
}
const int N = 80;
int main(int argc, char *argv[])
{
	//ifstream fin("ab.csv");
	//ifstream fin("Crimes_-_2001_to_present.csv");
	string s;
	ifstream fin(argv[1]);//"../amy/h100M.csv");
	getline(fin, s);
	char field[AA][BB] = {};
	splitstrbycomma(s.c_str(), s.size() + 1, field);
	size_t fieldlen = 0;
	int size, remainder;
	unsigned long long file_offset[TNUM];
	while (field[fieldlen][0])
		++fieldlen;
	
	vector<string> vrecords;
	while (getline(fin, s)) {
		vrecords.push_back(s);
	}
	
	ofstream fout("ab_para.json");
	fout << "[\n";

	// TODO: parallel this for loop
	size = vrecords.size() / TNUM;
	remainder = vrecords.size() % TNUM;
printf("$%d, %d\n", size, remainder);
	char **res = (char **)malloc(sizeof(char*) * TNUM);
	memset(res, 0, sizeof(char*) * TNUM);
	for(int i = 0; i < TNUM; i++){
		if(i == TNUM-1){
			res[i] = (char *)malloc(sizeof(char) * N * (size + remainder));
			memset(res[i], '\0', sizeof(char) * N * (size+remainder));
		}
		else{
			res[i] = (char *)malloc(sizeof(char) * N * size );
			memset(res[i], '\0', sizeof(char) * N * size);
		}
	}
auto start = chrono::system_clock::now();
	omp_set_num_threads(TNUM);
	#pragma omp parallel
	{
		int id, nthrds, res_idx, my_start, my_end, idx;
		id = omp_get_thread_num();
		size = vrecords.size() / TNUM;
		remainder = vrecords.size() % TNUM;
		char *ptr = res[id];
		my_start = id * size;
		idx = 0;
		if(id == TNUM-1)
			size += remainder;		
		my_end = my_start + size;
printf("(%d)[%d,%d'\n", id, my_start, my_end);
		for (int i = my_start; i < my_end; i++) {
			char json[CC] = {};
			res_idx = 0;
			if (i){
				strncpy(ptr, ",\n", 2);
				ptr = ptr + 2;
			}
				
			size_t jsonlen = csv2json(vrecords[i].c_str(), vrecords[i].size() + 1, field, fieldlen, ptr);//json);
			//fout << json;
			//strncpy(ptr, json, jsonlen);
			ptr = ptr + jsonlen;
		}
		file_offset[id] = ptr - res[id];
		//strcat(ptr, "\0");

if (id == 0) {
	auto end = chrono::system_clock::now();
	chrono::duration<double> diff = end - start;
	cout << "time: " << diff.count() << "s" << endl;
}
	}
	for(int i = 0; i < TNUM; i++){
			//fout << res[i];
			fout << res[i];
	}

	fout << "\n]\n";
	fin.close();
	fout.close();
	for(int i = 0; i < TNUM; i++){
		free(res[i]);
	}

	return 0;
}
